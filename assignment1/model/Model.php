<?php
include_once("Book.php");
include_once("IModel.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
 
 /** Author note, OBLIG 1
 * @author Daniel Siqveland
 * Collaboration with Kristoffer Baardseth
 * Built on Rune Hjelsvold's framework.
 * See comment above
 */
class Model implements IModel
{								  
    /**
	 * @todo The session array for storing book collection is to be replaced by a database.
	 */
	protected $db = null;
	 
	public function __construct($db = null)  
    {  
	    if ($db) {
			$this->db = $db;
		}
		else
		{
			try {
			    $this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
			    $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			    //echo 'Connected to database<br/>';
			} catch (PDOException $e) {
				echo 'Connection to database failed<br/>'. $e->getMessage();
			}
				
		}
	
	}
	
	/** Function returning the complete list of books in the collection. Books are
	 * returned in order of id.
	 * @return Book[] An array of book objects indexed and ordered by their id.
	 * @todo Replace implementation using a real database.
	 */
	 
	public function getBookList()
	{	
	    $books = array();
	    try {
			//Får feilmelding på $db her, udefinert?
			$stmt = $this->db->query('SELECT * FROM book ORDER BY id');
			$books = $stmt->fetchAll(PDO::FETCH_OBJ);
			return $books;
		} catch (PDOException $e) {
			echo 'Getting books failed<br/>' . $e->getMessage();
		}
	}
	
	/** Function retrieveing information about a given book in the collection.
	 * @param integer $id The id of the book to be retrieved.
	 * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @todo Replace implementation using a real database.
	 */
	public function getBookById($id)
	{
		if (!is_numeric($id)) {
		    echo "Not numeric data in ID";
		}
		
		try {
			$stmt = $this->db->prepare('SELECT * FROM book WHERE id = :id');
			$stmt->bindParam(':id', $id);
			$stmt->execute();
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($row) {
				$book = new Book ($row['title'], $row['author'], $row['description'], $row['id']);
				return $book;
			}
			else {
				return NULL;
			}
		} catch (PDOException $e) {
		    echo 'Failed getting book by id<br/>' . $e->getMessage();
		}
	}
	
	/** Adds a new book to the collection.
	 * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @todo Replace implementation using a real database.
	 */
	public function addBook($book)
	{
		if ($book->title && $book->author)
		{
			try {
				$stmt = $this->db->prepare('INSERT INTO book(title, author, description)
				VALUES (:title,:author,:description)');
				$stmt->bindValue(':title',$book->title);
				$stmt->bindValue(':author',$book->author);
				$stmt->bindValue(':description',$book->description);
				$stmt->execute();
				$book->id = $this->db->lastInsertId();
			} catch (PDOException $e) {
				echo 'Adding book failed<br/>' . $e->getMessage();
			}
			
		}
		else
			//Throw new PDOException("No Title/Author");
			echo 'Error: Did you forget to add Title/Author?';
	}

	/** Modifies data related to a book in the collection.
	 * @param Book $book The book data to kept.
	 * @todo Replace implementation using a real database.
	 */
	public function modifyBook($book)
	{
		if($book->title && $book->author) {
            try {
                $stmt = $this->db->prepare('UPDATE book SET title=:title, author=:author, description=:description WHERE id=' .
                $book->id);
                $stmt->bindValue(':title', $book->title, PDO::PARAM_STR);
                $stmt->bindValue(':author',$book->author, PDO::PARAM_STR);
                $stmt->bindValue(':description',$book->description, PDO::PARAM_STR);
                $stmt->execute();
                } catch(PDOException $e){
                $e->getMessage();
				}
        }
	}

	/** Deletes data related to a book from the collection.
	 * @param integer $id The id of the book that should be removed from the collection.
	 * @todo Replace implementation using a real database.
	 */
	public function deleteBook($id)
	{
		$delete = $this->db->prepare('DELETE FROM book WHERE id=:id');
		$delete->bindParam(':id', $id);
		$delete->execute();
		//echo "ID deleted was $id";
	}
	
	/** Helper function finding the location of the book in the collection array.
	 * @param integer $id The id of the book to look for.
	 * @return integer The index of the book in the collection array; -1 if the book is
	 *                 not found in the array.
	 */
	protected function getBookIndexById($id)
	{
		//Søker etter rad som inneholder $id i array $books, returnerer indeksen.
		$index = array_search($id, $books);
		return $index;
	}
	
	/** Helper function generating a sequence of ids.
	 * @return integer A value larger than the largest book id in the collection.
	 * @todo Replace with a call to a database auto_increment function.
	 */
	protected function nextId()
	{
		//Kanskje?
		$stmt = $this->db->prepare('SHOW TABLE STATUS LIKE book');
		$stmt->execute();
		return $stmt;
	}

}

?>